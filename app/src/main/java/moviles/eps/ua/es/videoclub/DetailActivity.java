package moviles.eps.ua.es.videoclub;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mastermoviles on 28/01/2017.
 */

public class DetailActivity extends AppCompatActivity {

    private String title;
    private String year;
    private String director;
    private boolean rented;
    private String synopsis;
    private Bitmap bitmap;
    private ImageView imageMovie_iv;
    private TextView title_tv,director_tv,year_tv,rented_tv,synopsis_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_main);
        imageMovie_iv = (ImageView)findViewById(R.id.imageMovie);
        title_tv = (TextView)findViewById(R.id.titleMovie);
        director_tv = (TextView)findViewById(R.id.dirMovie);
        year_tv = (TextView)findViewById(R.id.yearMovie);
        rented_tv = (TextView)findViewById(R.id.rentedMovie);
        synopsis_tv = (TextView)findViewById(R.id.synopsisMovie);
        synopsis_tv.setMovementMethod(new ScrollingMovementMethod());
        title = getIntent().getStringExtra(MainActivity.TITLE_ELEM);
        year = getIntent().getStringExtra(MainActivity.YEAR_ELEM);
        director = getIntent().getStringExtra(MainActivity.DIR_ELEM);
        rented = getIntent().getExtras().getBoolean(MainActivity.RENT_ELEM);
        synopsis = getIntent().getStringExtra(MainActivity.SYNOPSIS_ELEM);
        byte[] byteArray = getIntent().getByteArrayExtra(MainActivity.BITMAP_ELEM);
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imageMovie_iv.setImageBitmap(bitmap);
        title_tv.setText(title);
        director_tv.setText(director);
        year_tv.setText(year);
        rented_tv.setText(checkBooked(rented));
        synopsis_tv.setText(synopsis);

    }

    public String checkBooked(boolean rented){
        String booked = "Disponible";
        if(rented == true) {
            booked = "Alquilado";
        }

        return booked;
    }
}
