package moviles.eps.ua.es.videoclub;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 27/01/2017.
 */
public class MoviesList {

    public interface AsyncResponse {
        void processFinish(List<Film> output);
    }

    public static class DownloadHtmlTask extends AsyncTask<String,Void,List<Film>> {

        public AsyncResponse delegate = null;

        @Override
        protected List<Film> doInBackground(String[] url) {
            return getMovies(url[0]);
        }

        @Override
        protected void onPostExecute(List<Film> filmsList) {

            delegate.processFinish(filmsList);
        }
    }

    private static List<Film> getMovies(String url){

        HttpURLConnection http = null;
        List<Film> filmList = new ArrayList<>();

        try {
            URL uri = new URL(url);
            http = (HttpURLConnection)uri.openConnection();

            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");

            if(http.getResponseCode() == HttpURLConnection.HTTP_OK) {

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                JSONArray films = null;
                films = new JSONArray(sb.toString());
                http.disconnect();
                for(int i=0; i<films.length();i++){
                    JSONObject filmObj = films.getJSONObject(i);

                    String title = filmObj.getString("title");
                    String year = filmObj.getString("year");
                    String director = filmObj.getString("director");
                    String poster = filmObj.getString("poster");
                    URL urlPoster = new URL(poster);
                    HttpURLConnection connection = (HttpURLConnection) urlPoster.openConnection();
                    Bitmap bitmap = null;
                    if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        bitmap = BitmapFactory.decodeStream(connection.getInputStream());
                    }
                    int rentedNum = filmObj.getInt("rented");
                    boolean rented = false;
                    if(rentedNum == 1){
                        rented = true;
                    }
                    String synopsis = filmObj.getString("synopsis");
                    Film film = new Film(title,year,director,poster,rented,synopsis);
                    film.setBitmap(bitmap);
                    filmList.add(film);
                    connection.disconnect();
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally{
            if(http != null){
                http.disconnect();
            }
        }

        return filmList;
    }
}
