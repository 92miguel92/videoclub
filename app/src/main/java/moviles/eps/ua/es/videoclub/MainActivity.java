package moviles.eps.ua.es.videoclub;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements MoviesList.AsyncResponse{

    private ListView lista;
    private MovieAdapter movieAdapter;
    private MoviesList.DownloadHtmlTask asyncTask = new MoviesList.DownloadHtmlTask();
    public final static String TITLE_ELEM = "TITLE_ELEM";
    public final static String YEAR_ELEM = "YEAR_ELEM";
    public final static String DIR_ELEM = "DIR_ELEM";
    public final static String RENT_ELEM = "RENT_ELEM";
    public final static String SYNOPSIS_ELEM = "SYNOPSIS_ELEM";
    public final static String BITMAP_ELEM = "BITMAP_ELEM";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista = (ListView)findViewById(R.id.list_movies);
        try {
            asyncTask.delegate = this;
            asyncTask.execute("http://10.29.4.50/videoclub/public/api/v1/catalog").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Film movie = (Film) lista.getAdapter().getItem(position);
                intent.putExtra(TITLE_ELEM, movie.getTitle());
                intent.putExtra(YEAR_ELEM, movie.getYear());
                intent.putExtra(DIR_ELEM, movie.getDirector());
                intent.putExtra(RENT_ELEM, movie.isRented());
                intent.putExtra(SYNOPSIS_ELEM, movie.getSynopsis());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                movie.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                intent.putExtra(BITMAP_ELEM, byteArray);
                startActivity(intent);
            }
        });
    }

    @Override
    public void processFinish(List<Film> output) {
        movieAdapter = new MovieAdapter(getApplicationContext(), output);
        lista.setAdapter(movieAdapter);
    }
}
