package moviles.eps.ua.es.videoclub;

import android.content.Context;
import android.graphics.Movie;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mastermoviles on 26/01/2017.
 */
public class MovieAdapter extends BaseAdapter{

    private List<Film> mList;
    private Context context;
    private Map<String, Film> mapTL;
    private Boolean mBusy = false;

    public MovieAdapter(Context context, List<Film> mList) {
        this.mList = mList;
        this.context = context;
        mapTL = new HashMap<String, Film>();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.list_item, null);
        }

        TextView name_tv = (TextView)convertView.findViewById(R.id.nameMovie);
        ImageView image_tv = (ImageView)convertView.findViewById(R.id.imageMovie);

        Film movie = mList.get(position);
        name_tv.setText(movie.getTitle());
        image_tv.setImageBitmap(movie.getBitmap());

        return convertView;
    }
}
